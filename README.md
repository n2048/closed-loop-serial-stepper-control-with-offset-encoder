# closed loop serial stepper control with offset encoder

#Features: 
- Switch button disables the motor driver for manual positioning
- 20ppr rottary encoder allows offset and position adjustment
- Closed loop positioning using 600 ppr rotary encoder
- Receives target position through Serial at 115200 baud