
#define ENCODER_DO_NOT_USE_INTERRUPTS
#include <Encoder.h>

#define encoderA 2 // green
#define encoderB 3 // white

#define stepperPulsePin 4 // green
#define stepperDirectionPin 5 // white
#define stepperEnablePin 6 // blue

#define rotaryEncoderSwitchPin 7 // white
#define rotaryEncoderDataPin 8 // green
#define rotaryEncoderClkPin 9 // green

#define MOTOR_STEPS_PER_REV 200
#define PULSES_PER_REV 600
#define MICROSTEP 32

#define MOTOR_TEETH 20
#define PLATE_TEETH 40
#define ENCODER_TEETH 30

volatile boolean aFlag = LOW; // let's us know when we're expecting a rising edge on pinA to signal that the encoder has arrived at a detent
volatile boolean bFlag = LOW; // let's us know when we're expecting a rising edge on pinB to signal that the encoder has arrived at a detent (opposite direction to when aFlag is set)
volatile int encoderPos = 0; //this variable stores our current value of encoder position. Change to int or uin16_t instead of byte if you want to record a larger range than 0-255
int oldEncPos = 0; //stores the last encoder position value so we can compare to the current reading and see if it has changed (so we know when to print to the serial monitor)

volatile int targetPos = 0;
volatile int offsetPos = 0;

volatile boolean stepper_direction = LOW;
volatile boolean steper_pulse_state = LOW;

volatile boolean run_mode = HIGH;
boolean lastButtonState = HIGH;   // the previous reading from the input pin

int pulses_per_revolution = PULSES_PER_REV * PLATE_TEETH / ENCODER_TEETH;

Encoder myEnc(rotaryEncoderDataPin, rotaryEncoderClkPin);

void setup() {

  cli();

  pinMode(encoderA, INPUT_PULLUP); // set pinA as an input, pulled HIGH to the logic voltage (5V or 3.3V for most cases)
  pinMode(encoderB, INPUT_PULLUP); // set pinB as an input, pulled HIGH to the logic voltage (5V or 3.3V for most cases)

  attachInterrupt(digitalPinToInterrupt(encoderA), PinA, RISING); // set an interrupt on PinA, looking for a rising edge signal and executing the "PinA" Interrupt Service Routine (below)
  attachInterrupt(digitalPinToInterrupt(encoderB), PinB, RISING); // set an interrupt on PinB, looking for a rising edge signal and executing the "PinB" Interrupt Service Routine (below)

  TCCR0A = 0;// set entire TCCR0A register to 0
  TCCR0B = 0;// same for TCCR0B
  TCNT0  = 0;//initialize counter value to 0
  OCR0A = 124;// = (16*10^6) / (2000*64) - 1 (must be <256)
  TCCR0A |= (1 << WGM01);
  TCCR0B |= (1 << CS01) | (1 << CS00);
  TIMSK0 |= (1 << OCIE0A);

  pinMode(stepperPulsePin, OUTPUT);
  pinMode(stepperDirectionPin, OUTPUT);
  pinMode(stepperEnablePin, OUTPUT);

  pinMode(rotaryEncoderSwitchPin, INPUT);

  sei();

  digitalWrite(stepperEnablePin, run_mode);
  digitalWrite(stepperDirectionPin, LOW);
  digitalWrite(stepperPulsePin, LOW);

  Serial.begin(115200);
}


void loop() {
  if (oldEncPos != encoderPos) {
    Serial.print("");
    Serial.print("Current Position");
    Serial.print("\t");
    Serial.println(encoderPos + offsetPos);
  }
  oldEncPos = encoderPos;

  if (Serial.available()) {
    // accepts values between -32768 and 32767
    targetPos = Serial.parseInt();
    Serial.print("");
    Serial.print("Received Position");
    Serial.print("\t");
    Serial.println(targetPos);
  }

}

void PinA() {
  cli(); //stop interrupts happening before we read pin values
  byte reading = PIND & 0xC; // read all eight pin values then strip away all but pinA and pinB's values
  if (reading == B00001100 && aFlag) { //check that we have both pins at detent (HIGH) and that we are expecting detent on this pin's rising edge
    encoderPos --; //decrement the encoder's position count
    bFlag = 0; //reset flags for the next turn
    aFlag = 0; //reset flags for the next turn
  }
  else if (reading == B00000100) bFlag = 1; //signal that we're expecting pinB to signal the transition to detent from free rotation
  sei(); //restart interrupts
}

void PinB() {
  cli(); //stop interrupts happening before we read pin values
  byte reading = PIND & 0xC; //read all eight pin values then strip away all but pinA and pinB's values
  if (reading == B00001100 && bFlag) { //check that we have both pins at detent (HIGH) and that we are expecting detent on this pin's rising edge
    encoderPos ++; //increment the encoder's position count
    bFlag = 0; //reset flags for the next turn
    aFlag = 0; //reset flags for the next turn
  }
  else if (reading == B00001000) aFlag = 1; //signal that we're expecting pinA to signal the transition to detent from free rotation
  sei(); //restart interrupts
}

ISR(TIMER0_COMPA_vect) { //timer0 interrupt 2kHz toggles pin 8

  boolean sw = digitalRead(rotaryEncoderSwitchPin);

  int ctrEncPos = myEnc.read();

  if (sw != run_mode) {
    if ( sw ) {
      encoderPos = 0;
      targetPos = 0;
      offsetPos = ctrEncPos;
    }
    delay(1);
  }

  run_mode = sw;

  int target = targetPos - floor((ctrEncPos - offsetPos) / 2);
  int dif = abs(encoderPos - target);

  if (run_mode && dif > 1) {
    stepper_direction = encoderPos < target;
    steper_pulse_state = !steper_pulse_state;
    digitalWrite(stepperDirectionPin, stepper_direction);
    digitalWrite(stepperPulsePin, steper_pulse_state);
  }

  digitalWrite(stepperEnablePin, !run_mode);
}
